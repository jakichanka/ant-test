import { AUTH_LOGIN, AUTH_LOGOUT } from '../constants'

export const initialState = {
    isAuth: false,
}

interface Action {
    type: string
    payload: any
}

export const authReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case AUTH_LOGIN:
            return { ...state, isAuth: true }
        case AUTH_LOGOUT:
            return { ...state, isAuth: false }
        default:
            return state
    }
}
