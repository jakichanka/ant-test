import axios from 'axios'
import history from './history'
import Cookies from "universal-cookie";

const instance = axios.create({
    baseURL: 'http://gallery.dev.webant.ru',
    timeout: 10000,
    withCredentials: true,
})
const cookies = new Cookies()

instance.interceptors.request.use( (config) => {
    const token = cookies.get('access_token')
    config.headers.Authorization =  `Bearer ${token}`
    return config
    }
)

instance.interceptors.response.use(
    (response) => response,
    async (error) => {
        if (error.response && error.response.status === 401) {
            try {
                if (!localStorage.getItem('refresh_token')) return Promise.reject(error)
                const result = await instance.get('/oauth/v2/token', {
                    params: {
                        client_id: localStorage.getItem('client_id'),
                        grant_type: 'refresh_token',
                        refresh_token: localStorage.getItem('refresh_token'),
                        client_secret: localStorage.getItem('client_secret'),
                    },
                })
                cookies.set('access_token', result.data.access_token)
                return Promise.resolve(true)
            } catch (err) {
                localStorage.removeItem('refresh_token')
                history.push('/logout')
            }
        }
        return Promise.reject(error)
    },
)

export default instance
