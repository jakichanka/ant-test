import React, {useEffect, useState} from 'react'
import {List, Modal, Row, Typography} from 'antd'
import axios from '../utils/axios'
import isEmpty from 'lodash/isEmpty'
import {useRouter} from '../hooks/useRouter'
import {LazyLoadImage} from 'react-lazy-load-image-component'

import Card from '../components/Card'

const {Title, Text} = Typography;

interface PhotoData {
    dateCreate: string
    description: string
    id: number
    image: { id: number; name: string }
    name: string
    new: boolean
    popular: boolean
}

interface Photo {
    countOfPages?: number
    itemsPerPage?: number
    totalItems?: number
    data?: Array<PhotoData>
}

interface Modal {
    img: string
    desc: string
    title: string
}

const New = () => {
    const {history, location} = useRouter()

    const [page, setPage] = useState<number>(1)
    const [start, setStart] = useState<string>('1')
    const [limit, setLimit] = useState<string>('15')

    const [photos, setPhotos] = useState<Photo>({})
    const [loading, setLoading] = useState<boolean>()

    const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
    const [modal, setModal] = useState<Modal>({
        img: '',
        desc: '',
        title: ''
    })

    const showModal = (img: string, desc: string, title: string) => {
        setIsModalVisible(true);
        setModal({...modal, img: img, desc: desc, title: title})
    };

    const handlePage = (pageNum: number) => {
        setPage(pageNum)

        history.push({
            pathname: '/',
            search: "?" + new URLSearchParams({page: `${pageNum}`, start: start, limit: limit}).toString()
        })
    }

    useEffect(() => {
        const query = new URLSearchParams(location.search)

        setPage(prevState => parseInt(query.get('page') as string) || page)
        setLimit(prev => query.get('limit') || prev)
        setStart(prev => query.get('start') || prev)
    })

    useEffect(() => {
        const query = new URLSearchParams(location.search)

        const getPhotos = async () => {
                if (location.search === '') {
                    history.push({
                        pathname: '/',
                        search: "?" + new URLSearchParams({page: `${page}`, start: start, limit: limit}).toString()

                    })
                    return await axios.get('/api/photos', {
                        params: {
                            page: 1,
                            start: 1,
                            limit: 15,
                        },
                    })
                } else {
                    return await axios.get('/api/photos', {
                        params: {
                            page: query.get('page'),
                            start: query.get('start'),
                            limit: query.get('limit'),
                        },
                    })
                }
            }

        ;(async () => {
            try {
                setLoading(true)
                const photos = await getPhotos()
                console.log(photos)
                setLoading(false)
                if (photos?.data) {
                    setPhotos(photos.data)
                }
            } catch (err) {
                console.log(err)
            }
        })()
    }, [page])
    return (
        <Row>
            {!isEmpty(photos) &&
            <>
                <List
                    grid={{
                        gutter: 25,
                        xs: 1,
                        sm: 2,
                        md: 3,
                        lg: 4,
                        xl: 5,
                        xxl: 6
                    }}
                    loading={loading}
                    pagination={{
                        current: page,
                        onChange: (page: number) => handlePage(page),
                        showSizeChanger: false,
                        style: {textAlign: 'center'},
                        position: "bottom",
                        total: photos.totalItems,
                        pageSize: photos.itemsPerPage
                    }}
                    dataSource={photos.data}
                    renderItem={photo => (
                        <List.Item style={{marginBottom: '35px'}}>
                            <Card
                                hoverable={true}
                                onClick={() => showModal('http://gallery.dev.webant.ru/media/' + photo.image.name, photo.description, photo.name)}
                                bordered={false}
                                key={photo.id}
                                loading={true}

                                style={{height: '200px'}}
                                cover={
                                    <LazyLoadImage
                                        alt={photo.image.name}
                                        height={200}
                                        width={'100%'}
                                        effect='blur'
                                        src={'http://gallery.dev.webant.ru/media/' + photo.image.name}
                                    />
                                }
                            />
                        </List.Item>
                    )}
                />
                <Modal
                    centered
                    visible={isModalVisible}
                    onOk={() => setIsModalVisible(false)}
                    onCancel={() => setIsModalVisible(false)}
                    width={720}
                    closable={false}
                    style={{minWidth: '320px'}}
                    footer={null}
                >
                    <LazyLoadImage
                        src={modal.img}
                        width={'100%'}
                        effect='blur'
                        style={{maxHeight: '500px'}}
                    />
                    <div style={{fontFamily: 'Acrom'}}>
                        <Title style={{margin: '16px 0 20px 0', color: '#2F1767', fontWeight: 400}} level={5}>
                            {modal.title}
                        </Title>
                        <Text style={{color: '#606266'}}>
                            {modal.desc}
                        </Text>
                    </div>
                </Modal>
            </>
            }
        </Row>
    )
}

export default New
