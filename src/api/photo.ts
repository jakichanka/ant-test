import { toast } from 'react-toastify'
import axios from '../utils/axios'

export const getPhotos = async () => {
    const result = await axios.get('/api/photos')
    return result
}
