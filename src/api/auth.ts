import { toast } from 'react-toastify'
import axios from '../utils/axios'
import Cookies from 'universal-cookie'

export const createUser = async (values: any) => {
    try {
        await axios.post('/api/users', {
            email: values.email,
            phone: values.phone,
            fullName: values.fullName,
            password: values.password,
            username: values.username,
            birthday: values.birthday,
            roles: values.roles,
        })
        toast.success('Sign Up successfully')
    } catch (err) {
        if (err.response?.data?.detail) {
            err.response.data.detail.split('\n').forEach((el: string) => toast.error(el))
        }
    }
}

export const getClient = async () => {
    try {
        const result = await axios.post('/api/clients', {
            name: 'Zheka',
            allowedGrantTypes: ['password', 'refresh_token'],
        })
        return result
    } catch (err) {
        toast.error('Something went wrong')
    }
}

export const login = async (values: any) => {
    const result = await axios.get('/oauth/v2/token', {
        params: {
            client_id: localStorage.getItem('client_id'),
            grant_type: 'password',
            username: values.username,
            password: values.password,
            client_secret: localStorage.getItem('client_secret'),
        },
    })
    return result
}

export const getTokens = async (values: any, setAuthTrue: any) => {
    try {
        const client = await getClient()
        if (client?.data) {
            localStorage.setItem('client_id', client.data.id + '_' + client.data.randomId)
            localStorage.setItem('client_secret', client.data.secret)
            const result = await login(values)
            if (result?.data) {
                console.log(result.data)
                const cookies = new Cookies()
                console.log(result)
                cookies.set('access_token', result.data.access_token, {
                    path: '/',
                    expires: new Date(Date.now() + (result.data.expires_in * 1000) * 4),
                })
                localStorage.setItem('refresh_token', result.data.refresh_token)
                setAuthTrue()
            }
        }
    } catch (err) {
        if (err.response?.data) {
            toast.error(err.response.data.error_description)
        } else {
            toast.error('Something went wrong')
        }
    }
}
