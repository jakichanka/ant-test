import React from 'react'
import { Menu } from 'antd'
import { LinkStyled as Link } from './NavbarElements'
import { useRouter } from '../../hooks/useRouter'

const Navbar = () => {
    const { location } = useRouter()
    console.log(location.pathname)
    return (
        <Menu mode='horizontal' defaultSelectedKeys={[location.pathname]} selectedKeys={[location.pathname]} >
            <Menu.Item key='/'>
                <Link to='/'>New</Link>
            </Menu.Item>
            <Menu.Item key='/popular'>
                <Link to='/popular'>Popular</Link>
            </Menu.Item>
            <Menu.Item key='/logout'>
                <Link to='/logout'>Logout</Link>
            </Menu.Item>
            <Menu.Item key='/upload'>
                <Link to='/upload'>Upload</Link>
            </Menu.Item>
        </Menu>
    )
}

export default Navbar