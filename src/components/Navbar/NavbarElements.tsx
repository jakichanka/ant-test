import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const LinkStyled = styled(Link)`
    padding: 0 15px;
    font-family: 'Acrom', sans-serif;
`
